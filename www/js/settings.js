function _onLoad() {
	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//hide EXTERNALFILE items if file system is not accessible
	if ( typeof cordova !== 'undefined' ) {
		try {
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) { 
				fs.root.getFile(".accessdummy"+Date.now(), { create: true, exclusive: false }, function (fileEntry) {
					fileEntry.remove(function(){
						console.log("file is accessible");
					}, function () {
						console.log("file cant be removed, though");
					});
				}, function (err) {
					document.querySelectorAll('.EXTERNALFILE').forEach(externalfileitem => externalfileitem.hidden = true);
				})
			}, function (err) {
				document.querySelectorAll('.EXTERNALFILE').forEach(externalfileitem => externalfileitem.hidden = true);
			});
		} catch (err) {
			document.querySelectorAll('.EXTERNALFILE').forEach(externalfileitem => externalfileitem.hidden = true);
		}
	}
	//get last entry and mind index
	fillTopBar();
	
	//add event listeners
	document.querySelector('.setting.back').addEventListener('click', _onBack,false);
	document.querySelector('.setting.darkmode').addEventListener('click', _onDarkMode,false);
	document.querySelector('.setting.remindme').addEventListener('click', _onRemindMe,false);
	document.querySelector('.setting.export').addEventListener('click', _onExport,false);
	document.querySelector('.setting.import').addEventListener('click', _onImport,false);
	document.querySelector('.setting.passwd').addEventListener('click', _onPassword,false);

	//check checkbox if necessary
	if ( window.localStorage.getItem('setting_darkmode') == 1 ) { document.querySelector('.setting.darkmode').checked = true; }
	if ( window.localStorage.getItem('setting_remindme') == null || window.localStorage.getItem('setting_remindme') == 1 ) { document.querySelector('.setting.remindme').checked = true; }

	//show body
	document.body.style.opacity = "1";
}

function _onDarkMode() {
	if ( document.querySelector('.setting.darkmode').checked ) {
		window.localStorage.setItem('setting_darkmode',"1");
	} else  {
		window.localStorage.setItem('setting_darkmode',"0");
	}
	parseSettings();
	backup(true);
}

function _onRemindMe() {
	if ( document.querySelector('.setting.remindme').checked ) {
		window.localStorage.setItem('setting_remindme',"1");
	} else  {
		window.localStorage.setItem('setting_remindme',"0");
	}
	parseSettings();
	backup(true);
}

function _onPassword(e) {
	e.preventDefault();
	if ( ! window.sessionStorage.getItem("key") ) { window.sessionStorage.setItem('key','none'); } //signals that the user wants to set a password now
	window.location = "encrypt.html";
}

function _onExport(e) {
	e.preventDefault();
	let download = document.createElement("a");
	download.href = "data:text/plain;charset=UTF-8;base64,";
	download.setAttribute("download","HabsGleich-Sicherung-"+Date.now());
	download.textContent = strings["downloadhere"];
	let stringLS = stringifyLocalStorage();
	download.href += btoa(stringLS);
	download.click(); export_success(true);
	if ( typeof cordova !== 'undefined' ) {
		function saveBackup() {
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
				fs.root.getFile("HabsGleich-Sicherung-"+Date.now()+".txt", { create: true, exclusive: false }, function (fileEntry) {
					fileEntry.createWriter(writer => {
						writer.onwriteend = function(e) { 
							export_success(true);
						};
						writer.onerror = function(e) { 
							export_success(false);
						};
						writer.write(stringLS);
					},function (err) { console.log('error getting file! ' + JSON.stringify(err)); export_success(false,stringLS); });
				}, function (err) { console.log('error getting file! ' + JSON.stringify(err)); export_success(false,stringLS); });
			}, function (err) { console.log('error getting persistent fs! ' + JSON.stringify(err)); export_success(false,stringLS); });
		}
		let permissions = cordova.plugins.permissions;
		permissions.checkPermission(permissions.WRITE_EXTERNAL_STORAGE,saveBackup, function() { 
			export_success(false,stringLS);
			permissions.requestPermission(permissions.WRITE_EXTERNAL_STORAGE,saveBackup, function() {
				export_success(false,stringLS);
			}); 
		});
	}
}

function export_success(bool,exportedText) {
	if ( bool ) {
		document.querySelector(".setting_result.export").classList.remove("failed");				
		document.querySelector(".setting_result.export").classList.add("success");				
		document.querySelector(".setting_result.export").textContent = strings["exportsuccessful"];				
	} else {
		document.querySelector(".setting_result.export").classList.remove("success");				
		document.querySelector(".setting_result.export").classList.add("failed");				
		document.querySelector(".setting_result.export").textContent = strings["exportfailed"];
		//~ //create clipboard button
		//~ let clipbtn = document.createElement('div');
		//~ clipbtn.classList.add('clip','btn','small');
		//~ clipbtn.dataset.content = exportedText;
		//~ document.querySelector(".setting_result.export").appendChild(clipbtn);
		//~ document.querySelector(".clip.btn").addEventListener("click",copyToClipboard);				
	}
	setTimeout(() => {
		document.querySelector(".setting_result.export").textContent = "";
		document.querySelector(".setting_result.export").classList.remove("failed","success");
	}, 5000);
}

function copyToClipboard(event) {
	//does not work in android (without further permissions?)
	navigator.clipboard.writeText(event.target.dataset.content).then(() => {
		document.querySelector('.clip.btn').remove;
		document.querySelector(".setting_result.export").textContent = "";		
	});
}

function pasteFromClipboard(event) {
	navigator.clipboard.readText().then( res => fillLocalStorage(res) );
}

function debug_log(string) {
	document.querySelector('.debug').innerHTML += string+"<br />";
}

function _onImport(e,checkCordova) {
	e.preventDefault();
	if ( typeof cordova != 'undefined' && typeof checkCordova == 'undefined') {
		let permissions = cordova.plugins.permissions;
		permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, function() { _onImport(new Event("click"),true); return true; }, function() { 
			permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE, function() { _onImport(new Event("click"),true); return true; }, function() {
				import_success(false);
			});
			return true; 
		});		
	}
	//create buttons for clipboard import and file upload...
	//~ //create clipboard button
	//~ if ( ! document.querySelector('.clip.btn') ) {
		//~ let clipbtn = document.createElement('div');
		//~ clipbtn.classList.add('clip','btn');
		//~ clipbtn.textContent = strings['importfromclipboard'];
		//~ document.querySelector(".setting_result.import").appendChild(clipbtn);
		//~ document.querySelector(".clip.btn").addEventListener("click",pasteFromClipboard);
	//~ }
	//create file button
	if ( ! document.querySelector('.file.btn') ) {
		let filebtn = document.createElement('div');
		filebtn.classList.add('file','btn');
		filebtn.textContent = strings['importfromfile'];
		document.querySelector(".setting_result.import").appendChild(filebtn);
		document.querySelector(".file.btn").addEventListener("click",importFromFile);
	}
}

function importFromFile() {
	let upload = document.createElement("input");
	upload.type = "file";
	upload.setAttribute("hidden",true);
	document.querySelector('.debug').appendChild(upload);
	upload = document.querySelector('.debug input');
	upload.addEventListener("change",_onUploadFile);
	setTimeout(function(){upload.click();},100); //should be enough for the EventListener to be set before click; any other ideas?
}

function _onUploadFile(e) {
	let file = e.target.files[0];
	if (file) {
		const fileReader = new FileReader();
		fileReader.onloadend = function() {
			fillLocalStorage(this.result);
			document.querySelector('.debug input').remove();
		}
		fileReader.onerror = function (ev) { import_success(false); }
		fileReader.readAsText(file,'ISO-8859-1');
	}
}

function import_success(bool) {
	if (bool) {
		document.querySelector(".setting_result.import").classList.remove("failed");				
		document.querySelector(".setting_result.import").classList.add("success");				
		document.querySelector(".setting_result.import").textContent = strings["importsuccessful"];				
	} else {
		document.querySelector(".setting_result.import").textContent = strings["importfailed"];
		document.querySelector(".setting_result.import").classList.add("failed");				
		document.querySelector(".setting_result.import").classList.remove("success");				
	}
	setTimeout(() => {
		document.querySelector(".setting_result.import").textContent = "";
		document.querySelector(".setting_result.import").classList.remove("failed","success");
	}, 5000);
}

//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
