async function _onLoad() {
	//load master key
	let control = await makeMasterKeyAvailable();

	//hide submit until puzzle is shown
	document.querySelector('.continueIndex').style.visibility = "hidden";
	document.querySelector('.chunkContainer').style.visibility = "hidden";

	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();
	
	//add listeners and reset form
	document.querySelector('#testForm').addEventListener('submit',_onSubmitTestForm,false);
	document.querySelector('#continueForm').addEventListener('submit',_onSubmitContinueForm,false);
	document.querySelector('#testForm').reset();

	//get test item and fill test container
	let testtimestamp = testAdventure("forTest");
	if ( ! testtimestamp ) { 
		document.querySelector('.before').textContent = strings['sorrynotest'];
		//show body
		document.body.style.opacity = "1";
		return false;
	}

	document.querySelector('.before').addEventListener('click',_onClickBefore,false);
	
	let teststring = await _getString(testtimestamp);
//	let teststring = window.localStorage.getItem("created_"+testtimestamp);
//	let teststring = "Der Hund lief mir heute über den Weg. Dieser blöde Hund.";
//	let testdate = new Date(testtimestamp);
	let testdatestring = timestamp2readable(testtimestamp);
	if ( testdatestring.indexOf('.') > -1 ) {
		document.querySelector('.testcreated').textContent = strings['on']+' '+testdatestring+":";
	} else {
		document.querySelector('.testcreated').textContent = testdatestring+":";
	}
	
	let essentialwords = essentialWords(teststring);
	let testword = [];
	let rnd1 = Math.floor(Math.random()*essentialwords.length);
	let testword1 = essentialwords[rnd1];
	essentialwords.splice(rnd1,1);
	let rnd2;
	let testword2 = testword1;
	while ( testword1 == testword2 ) {
		rnd2 = Math.floor(Math.random()*essentialwords.length);
		testword2 = essentialwords[rnd2];
	}
	//sort testwords correctly
	let chunk = [];
	let chunk1 = teststring.split(testword1);
	chunk1 = [chunk1.shift(),chunk1.join(testword1)];
	if ( chunk1[0].indexOf(testword2) > -1 ) {
		chunk = chunk1[0].split(testword2);
		chunk = [chunk.shift(),chunk.join(testword2)];
		chunk[2] = chunk1[1];
		testword = [testword2,testword1];
	} else {
		chunk[0] = chunk1[0];
		let chunk3 = chunk1[1].split(testword2);
		chunk3 = [chunk3.shift(),chunk3.join(testword2)];
		chunk[1] = chunk3[0];
		chunk[2] = chunk3[1];
		testword = [testword1,testword2];
	}

	function escapeRegExp(string) {
		return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
	}

	let rx = new RegExp(escapeRegExp(testword1)+'\|'+escapeRegExp(testword2));
	chunk = [chunk[0],chunk[1]].concat(chunk[2].split(rx));
	let tmpchunk = JSON.parse(JSON.stringify(chunk));
	tmpchunk.splice(0,2);
	chunk[2] = tmpchunk.join(' *** ');
	document.querySelector('.chunk1').textContent = chunk[0];
	document.querySelector('.chunk2').textContent = chunk[1];
	document.querySelector('.chunk3').textContent = chunk[2];
		
	//save correct answers temporarily:
	window.sessionStorage.setItem("forHere",JSON.stringify([testword[0],testword[1],teststring,essentialwords.length,testtimestamp]));
	
	window.sessionStorage.setItem("page","test.html");

	//show body
	document.body.style.opacity = "1";
}

function _onClickBefore(e) {
	document.querySelector('.skipIndex').style.visibility = "hidden";
	document.querySelector('.continueIndex').style.visibility = "visible";
	document.querySelector('.chunkContainer').style.visibility = "visible";
	e.target.style.display = "none";
}

function _onSubmitTestForm(e) {
	e.preventDefault();	
	if ( testSubmitted > 0 ) { document.body.display = "none"; window.location = "results.html"; return false; }
	testSubmitted = 1;
	const teststring = [document.querySelector('.test1').value,document.querySelector('.test2').value];
	/* solution: indices
	 * 0: word1
	 * 1: word2
	 * 2: full string
	 * 3: number of essential words
	 * 4: creation timestamp
	*/
	const solution = JSON.parse(window.sessionStorage.getItem("forHere"));
	
	let remembered = [];
	let called = Date.now();
	let words = solution[3];
	let created = solution[4];
	// similarity of 0.4 is enough to be plausibly remembered
	for ( i = 0; i < 2; i++ ) {
		remembered[i] = false;
		if ( similarity(teststring[i]+' ',solution[i]+' ') > 0.4 ) { remembered[i] = true; }
	}
	let remembered_total = (remembered[0]+remembered[1])/2;
	//display correct version
	if ( ! remembered[0] ) {
		document.querySelector('.test1').style.textDecoration = "line-through";
		document.querySelector('.test1').disabled = true;
		document.querySelector('.chunk1').innerHTML += " <span class=\"correct\">"+solution[0]+"</span>";
	} else {
		document.querySelector('.test1').classList.add('correct');
		document.querySelector('.test1').value = solution[0];
		document.querySelector('.test1').disabled = true;
	}
	if ( ! remembered[1] ) {
		document.querySelector('.test2').style.textDecoration = "line-through";
		document.querySelector('.test2').disabled = true;
		document.querySelector('.chunk2').innerHTML += " <span class=\"correct\">"+solution[1]+"</span>";
	} else {
		document.querySelector('.test2').classList.add('correct');
		document.querySelector('.test2').value = solution[1];
		document.querySelector('.test2').disabled = true;
	}
	
	//write to tmp for next page to use
	let mI = document.querySelector('.mindindexvalue').textContent;
	window.sessionStorage.setItem("forResults",JSON.stringify([remembered_total,mI]));
	//update localStorages after test:
	window.localStorage.setItem("lastcallof_"+created,called);
	window.localStorage.setItem("historyof_"+created,JSON.stringify(JSON.parse(window.localStorage.getItem("historyof_"+created)).concat([called])));
	let newMetaItem = { words: words, remembered: remembered_total, called: called, created: created };
	window.localStorage.setItem("called_"+called,JSON.stringify(newMetaItem));
}

function _onSubmitContinueForm(e) {
	e.preventDefault();
	backup(true, function(){ window.location = "summary.html"; });
}

var testSubmitted = 0;

//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
