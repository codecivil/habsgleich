function _onLoad() {
	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();	

	//show body
	document.body.style.opacity = "1";
	
	//set page name
	window.sessionStorage.setItem("page","info.html");

}

//imprint must be accessible before login:
if ( typeof cordova !== 'undefined' ) {
	window.addEventListener('DOMContentLoaded', function () {
		document.addEventListener('deviceready',_onLoad, false);
	}, false);
} else {
	window.addEventListener('DOMContentLoaded',_onLoad, false);
}
