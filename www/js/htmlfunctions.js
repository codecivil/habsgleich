//HTML functions

function fillHTML() {
	//create menu
	// //menu element
	let menuEl = document.querySelector('.burgermenu .menuitems');
	// //define it here
	let menuObj = [
		{ "menu_main": [
			{ "name": "adventure", "href": "index.html" },
			{ "name": "test", "href": "test.html" },
			{ "name": "diary", "href": "diary.html" },
			{ "name": "statistics", "href": "statistics.html" },
			{ "name": "summary", "href": "summary.html" }
		]},
		{ "menu_settings": [
			{ "name": "settings", "href": "settings.html" }
		]},
		{ "menu_logout": [
			{ "name": "logout", "href": "logout.html", "classes": "NOCRYPTO" }
		]},
		{ "menu_formal": [
			{ "name": "imprint", "href": "impressum.html" },
			{ "name": "dataprotection", "href": "datenschutz.html", "classes": "BROWSERONLY" }
		]}
	]
	// //
	menuObj.forEach( section => {
		for ( sectionname of Object.keys(section) ) {
			let sectiondiv = document.createElement('span');
			sectiondiv.classList.add('menuitem');
			sectiondiv.classList.add(sectionname);
			let sectionul = document.createElement('ul');
			for ( menuitem of section[sectionname] ) {
				if ( document.URL.indexOf(menuitem.href) == -1 ) {
					let sectionli = document.createElement('li');
					if ( menuitem.classes != null ) {
						sectionli.setAttribute("class",menuitem.classes);
					}
					let sectiona = document.createElement('a');
					sectiona.dataset.class = menuitem.name;
					sectiona.setAttribute("href",menuitem.href);
					sectionli.append(sectiona);
					sectionul.append(sectionli);
				}
			}
			sectiondiv.append(sectionul);
			menuEl.append(sectiondiv);
		}
	})
	
	//fill data-class translations
	document.querySelectorAll('[data-class]').forEach(function(data_class){
		data_class.innerHTML = strings[data_class.dataset.class];
	});
	document.querySelectorAll('[data-placeholder]').forEach(data_placeholder => {
		data_placeholder.placeholder = strings[data_placeholder.dataset.placeholder];
	});
	//limit textarea size to 256 characters
	document.querySelectorAll('textarea').forEach(textarea => textarea.setAttribute('maxlength','256') );
	//hide NOCRYPTO elements if NO_CRYPTO is true
	if ( NO_CRYPTO ) {
		document.querySelectorAll('.NOCRYPTO').forEach( nogoogleplay => nogoogleplay.style.display = "none" );
	}
	//hide NOGOOGLEPLAY elements if GOOGLEPLAY is true
	if ( GOOGLEPLAY ) {
		document.querySelectorAll('.NOGOOGLEPLAY').forEach( nogoogleplay => nogoogleplay.style.display = "none" );
	}
	//hide BROWSERONLY elements for cordova platforms
	if ( typeof cordova != 'undefined' ) {
		document.querySelectorAll('.BROWSERONLY').forEach( browseronly => browseronly.style.display = "none" );
	}
	//make browser adjustments (e.g. insert Demo! caveat)
	if ( typeof cordova == 'undefined' ) {
		function browserspecific() {
			//show BROWSERONLY elements for online version
			if ( typeof cordova == 'undefined' ) {
				document.querySelectorAll('.BROWSERONLY').forEach( browseronly => browseronly.style.display = "initial" );
			}
			//append demo div
			let demo = document.createElement('div');
			demo.innerHTML = strings["demo"];
			demo.classList.add("demo");
			document.body.appendChild(demo);
			document.querySelector('.demo').addEventListener("click",function(ev){ev.target.remove();});
		}
		browserspecific();
	}
}

function fillTopBar() {
	let mI = mindIndex()[0];
	let _keys = Object.keys(localStorage).filter(key => (key.indexOf("created_") > -1 ) ).sort();
	if ( _keys == undefined || _keys.length == 0 ) { displayTopBar(mI,"n.a."); return false; }
	let created = parseInt(_keys[_keys.length-1].replace("created_",""));
	lE = timestamp2readable(created);
	displayTopBar(mI,lE);
}

function displayTopBar(mI,lE) {
	document.querySelector('.lastentryvalue').textContent = lE;
	if ( mI == 0 ) { 
		document.querySelector('.mindindexvalue').textContent = "n.a.";
	} else {
		lEstring = (Math.floor(mI*10+0.5)/10).toString();
		if ( lEstring.indexOf('.') == -1 ) { lEstring += '.0'; }
		document.querySelector('.mindindexvalue').textContent = lEstring;
	}
}

function _onBack() {
	window.location = window.sessionStorage.getItem("page");
}

/* String functions */
function essentialWords(string) {
	let essentialString = string;
	let essentialStringnew = ' '+string+' ';
	while ( essentialStringnew != essentialString ) {
		essentialString = essentialStringnew;
		essentialStringnew = essentialString.replace(/[ ,.;:][^ ,.:;]{1,3}[ ,.;:]/,' ');
	}
	for ( let blackword of blacklist ) {
		regex = new RegExp(' '+blackword+' ',"ig");
		essentialString = essentialString.replace(regex,' ');
	}
	essentialString = essentialString.replace(/[ ,.;:]/g,' ').replace(/[ ]{1,}/g,' ').replace(/^ /,'').replace(/ $/,'')	;
	//construct key values
	let words = essentialString.split(' ');
	return words;
}

/* Date functions */
function timestamp2readable(timestamp) {
	let date = new Date(timestamp);
//	let re = date.getDate()+'.'+(date.getMonth()+1)+'.'+date.getYear();
	let re = date.toLocaleDateString(navigator.languages[0]);
	if ( Date.now()-timestamp < 86400000*365 ) { re = re.replace(date.getFullYear(),''); }
	if ( Date.now()-timestamp < 86400000 && date.getDate() == new Date(Date.now()).getDate() ) { re = strings["today"]; }
	if ( Date.now()-timestamp < 172800000 && date.getDate() == new Date(Date.now()).getDate()-1 ) { re = strings["yesterday"]; }
	return re;
}

/* Parse settings */
function parseSettings() {
		// Dark Mode
		if ( window.localStorage.getItem('setting_darkmode') == 1 ) {
			document.querySelector('html').classList.add('dark');
			document.body.style.filter = "invert(96%)";
			document.body.style.background = "black";
		} else {
			document.querySelector('html').classList.remove('dark');
			document.body.style.filter = "";
			document.body.style.background = "white";
		}
		setInterval(function(){
			if ( window.localStorage.getItem('setting_remindme') == null || window.localStorage.getItem('setting_remindme') == 1 ) {
				//navigator.notification comes form a cordova plugin, so not available in browser
				if ( navigator.notification ) {
					navigator.notification.beep(2);
					navigator.notification.confirm(strings['reminder_text'],yesWhyNot,strings['reminder'],[strings['notnow'],strings['yeswhynot']]);			
				}
			}
		},REMINDME);
}

//Yes, Remind Me (only for cordova)
function yesWhyNot(buttonIndex) {
	if ( buttonIndex > 1 ) {
		window.location = "index.html";
	}
}


/* Crypto functions */

//make masterkey available
/* This has to be asynced: in android vm sometimes this is not yet available (and the global masterkey is user in mindfunctions.js: _getString etc.) */
var masterkey;
var masteriv;

async function makeMasterKeyAvailable() {
	if ( window.sessionStorage.getItem("key") && window.localStorage.getItem("crypto") ) {
		try {
			masterkey = await
			window.crypto.subtle.importKey(
				"jwk",
				JSON.parse(window.sessionStorage.getItem("key")),
				{
					name: "AES-GCM",
					length: 256
				},
				true,
				["encrypt", "decrypt"],
			);
			//.then (res => masterkey = res );
			masteriv = _array2buffer(JSON.parse(JSON.parse(window.localStorage.getItem("crypto")).masteriv));
		} catch (err) { console.log(err); }
	}
}
//
	
function _array2buffer(_array) {
	if ( typeof _array == "object" ) {
		let _realarray = [];
		Object.keys(_array).forEach( (_key) => { _realarray[_key] = _array[_key]; });
		_array = _realarray; 
	}
	let buffer = new ArrayBuffer(_array.length);
	let _Uint8Array = new Uint8Array(buffer);
	for ( i = 0; i < _array.length; i++ ) { _Uint8Array[i] = _array[i]; }
	return buffer;
}

/*
 * This is essentially from
 * https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/deriveKey
 * |
 * v
Get some key material to use as input to the deriveKey method.
The key material is a password supplied by the user.
*/
async function getKey(inputel,salt) {
  const password = inputel.value;
  const enc = new TextEncoder();
  const keyMaterial = await window.crypto.subtle.importKey(
    "raw",
    enc.encode(password),
    "PBKDF2",
    false,
    ["deriveBits", "deriveKey"],
  );
  const key = await window.crypto.subtle.deriveKey(
    {
      name: "PBKDF2",
      salt,
      iterations: 100000,
      hash: "SHA-256",
    },
    keyMaterial,
    { "name": "AES-GCM", "length": 256},
    true,
    ["encrypt", "decrypt"],
  );
  return key;
}

//plaintext: string; returns promise fulfilled by base64 string
async function encrypt(key,iv,plaintext) {
  let _arrayBuffer = await window.crypto.subtle.encrypt(
    { name: "AES-GCM", iv },
    key,
    new TextEncoder().encode(plaintext)
  );
  let _uint8Array = new Uint8Array(_arrayBuffer);
  let res = await buffer2base64(_uint8Array);
  return res;
//  return JSON.stringify(_uint8Array);
}

//ciphertext: base64 string, returns promise fulfilled by string
async function decrypt(key,iv,ciphertext) {
  try {
	  let cipherbuffer = base642buffer(ciphertext);
//	  let cipherbuffer = _array2buffer(JSON.parse(ciphertext));
	  let plainbuffer = await window.crypto.subtle.decrypt(
		{ name: "AES-GCM", iv },
		key,
		cipherbuffer
	  ); 
	  return new TextDecoder("utf-8").decode(plainbuffer);
	} catch (err) { return ciphertext; }
}

function buffer2base64(a) {
	return new Promise( (resolve,reject) => {
		let blob = new Blob([a]);
		let reader = new FileReader();
		let rx = new RegExp(/[^,]*,/);
		reader.onload = function(event){
		   resolve(event.target.result.replace(rx,''));
		};
		reader.readAsDataURL(blob);
	});
}

function base642buffer(b64) {
    var binary_string = window.atob(b64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

/* ^
 * |
 * This was essentially from
 * https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/deriveKey
*/

function logout(e) {
	e.preventDefault();
	window.sessionStorage.removeItem("key");
	window.location = "index.html";
}

//Debug functions

function debug_log(string) {
	document.querySelector('.debug').innerHTML += string+"<br />";
}

//does not yet return usable data...
function measurePace() {
	const start = Date.now();
	let end;
	let upload = document.createElement("input");
	upload.type = "file";
	upload.setAttribute("hidden",true);
	upload.addEventListener("custom",_setEnd);
	while ( typeof end == 'undefined' ) { upload.dispatchEvent(new Event("custom")); }
	function _setEnd() {
		end = Date.now();
		return end-start;
	}
	return end-start;
}

//todo: this has to be properly asynced! How to wait for writing to file before changing page?
//internal backup function (only cordova!)
// _bool: true for backup, false for restore
async function backup(_bool,callback) {
	if ( typeof cordova !== 'undefined' ) {
		function localSaveBackup() {
			window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (fs) {
		//	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
				fs.getFile("HabsGleich-Sicherung.txt", { create: true, exclusive: false }, function (fileEntry) {
					fileEntry.createWriter(writer => {
						writer.onwriteend = function(e) { 
							document.body.style.background = initialbodybackground;
							document.querySelector('.app').style.opacity = 1;
							if (callback) { return callback(); }
						};
						writer.write(stringifyLocalStorage());
					},function (err) { console.log('error writing to file! ' + JSON.stringify(err)); });
				}, function (err) { console.log('error getting file for writing! ' + JSON.stringify(err)); });
			}, function (err) { console.log('error getting persistent fs! ' + JSON.stringify(err)); });
		}
		function localRestoreBackup() {
			window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function (fs) {
		//	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
				fs.getFile("HabsGleich-Sicherung.txt", { create: true, exclusive: false }, function (fileEntry) {
					fileEntry.file(function (file) {
						var reader = new FileReader();
						reader.onloadend = function() {
							fillLocalStorage(this.result);
							document.body.style.background = initialbodybackground;
							document.querySelector('.app').style.opacity = 1;
							if (callback) { return callback(); }
						};
						reader.readAsText(file);
					}, function (err) { console.log('error reading file! ' + JSON.stringify(err)); });
				}, function (err) { console.log('error getting file for reading! ' + JSON.stringify(err)); });
			}, function (err) { console.log('error getting internal data directory! ' + JSON.stringify(err)); });
		}

		let initialbodybackground = document.body.style.background;
		document.body.style.background = 'center / contain no-repeat url(./img/justasec.svg)';
		document.querySelector('.app').style.opacity = 0;

		if ( _bool ) { console.log("localSave"); await localSaveBackup(); } else { console.log("localRestore"); await localRestoreBackup(); }

	} else { if (callback) { return callback(); } }
}

function fillLocalStorage(_jsonstring) {
	try { 
		let importObj = JSON.parse(_jsonstring);
		importObj.forEach(stored => {
			Object.keys(stored).forEach(key => {
				window.localStorage.setItem(key,stored[key]);
			});
		});
		if (typeof import_success == "function" ) { import_success(true); }
	} catch (err) {
		if (typeof import_success == "function" ) { import_success(false); }
	}
}

function objectifyLocalStorage() {
	let content = [];
	Object.keys(localStorage).forEach(item => {
		let pushObj = new Object();
		pushObj[item] = window.localStorage.getItem(item);
		content.push(pushObj);
	}); 
	return content;
}

function stringifyLocalStorage() { return JSON.stringify(objectifyLocalStorage()); }
