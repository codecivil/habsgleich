var original_string = '';

async function _onLoad() {
	//load masterkey
	let control = await makeMasterKeyAvailable();

	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();

	//set date headline
	let timestamp = parseInt(window.sessionStorage.getItem("edit"));
	let testdatestring = timestamp2readable(timestamp);
	if ( /\d/.test(testdatestring) ) {
		document.querySelector('.editdate').textContent = strings['of1']+' '+testdatestring+":";
	} else {
		document.querySelector('.editdate').textContent = strings['of2']+' '+testdatestring+":";
	}
//	document.querySelector('textarea').textContent = window.localStorage.getItem("created_"+timestamp);
	_getString(timestamp).then(res => { original_string = res; document.querySelector('textarea').textContent = res; });
	
	document.querySelector('#adventureForm').addEventListener('submit',_onSubmitAdventureForm,false);
	document.querySelector('#continueForm').addEventListener('submit',_onSubmitContinueForm,false);
	document.querySelector('#deleteForm').addEventListener('submit',_onSubmitDeleteForm,false);
	document.querySelector('.setting.back').addEventListener('click', _onSubmitContinueForm,false);

	//show body
	document.body.style.opacity = "1";
}

function _onSubmitAdventureForm(e) {
	e.preventDefault();	
	const timestamp = window.sessionStorage.getItem("edit");
	const adventure_string = document.querySelector('textarea[form=adventureForm]');
	let words = essentialWords(adventure_string.value).length;
	let called = Date.now();
	let remembered = 0;
	//insert into localStorage ("created", "called" and "lastcallof" prefixes)
	_putString(timestamp,adventure_string.value);
	//remove history if change was essential
	if ( similarity(original_string,adventure_string.value) < 0.4 ) {
		window.localStorage.setItem("lastcallof_"+timestamp,called);
		try { JSON.parse(window.localStorage.getItem("historyof_"+timestamp)).forEach(history => {
			window.localStorage.removeItem("called_"+history);
		}); } catch(err) {};
		window.localStorage.setItem("historyof_"+timestamp,JSON.stringify([timestamp,called]));
		let newMetaItem = { words: words, remembered: remembered, called: called, created: called };
		window.localStorage.setItem("called_"+timestamp, JSON.stringify(newMetaItem));
	}
	backup(true,toDiary);
	//what to do after successful inserts
}

function toDiary() { window.location = "diary.html"; }

function _onSubmitContinueForm(e) {
	e.preventDefault();
	window.location = window.sessionStorage.getItem("page");
}

function _onSubmitDeleteForm(e) {
	e.preventDefault();
	//navigator.notification comes form a cordova plugin, so not available in browser
	if ( navigator.notification ) {
		navigator.notification.confirm(strings['deletereally'],_delete,strings['confirmdelete'],[strings['no'],strings['yes']]);
	} else {			
		if ( confirm(strings["deletereally"]) ) { _delete(); }
	}
	function _delete(buttonIndex)	{
		if ( buttonIndex > 1 ) {
			const timestamp = window.sessionStorage.getItem("edit");
			window.localStorage.removeItem("created_"+timestamp);
			window.localStorage.removeItem("lastcallof_"+timestamp);
			try { JSON.parse(window.localStorage.getItem("historyof_"+timestamp)).forEach(history => {
				window.localStorage.removeItem("called_"+history);
			}); } catch(err) {};
			window.localStorage.removeItem("historyof_"+timestamp);
			backup(true,toDiary);
		}
	}
}

//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
