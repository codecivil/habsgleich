export function browserspecific() {
	//show BROWSERONLY elements for online version
	if ( typeof cordova == 'undefined' ) {
		document.querySelectorAll('.BROWSERONLY').forEach( browseronly => browseronly.style.display = "initial" );
	}
	//append demo div
	let demo = document.createElement('div');
	demo.innerHTML = strings["demo"];
	demo.classList.add("demo");
	document.body.appendChild(demo);
}
