async function newEntry(lag) {
	let xhr = new XMLHttpRequest();
//	xhr.open("GET","https://loripsum.net/api/1/short");
	xhr.open("GET","https://dinoipsum.com/api?format=text&paragraphs=1",true);
	xhr.onload = function() {
		if (xhr.readyState === xhr.DONE) {
			if (xhr.status === 200) {
				console.log(lag);
				let words = essentialWords(xhr.responseText).length;
				let remembered = 0;
				let called = Date.now() - parseInt(lag)*86400000;
				_putString(called,xhr.responseText);
				window.localStorage.setItem("lastcallof_"+called,called);
				window.localStorage.setItem("historyof_"+called,JSON.stringify([called]));
				let newMetaItem = { words: words, remembered: remembered, called: called, created: called };
				window.localStorage.setItem("called_"+called, JSON.stringify(newMetaItem));
			}
		}
	}
	xhr.send();
}

async function randomTest() {
	let willtest = Math.floor(Math.random()+0.75);
	if ( willtest == 0 ) { return; }
	let created = testAdventure("forTest");
	console.log("created",created);
	let called = created+5*86400000;
	let remembered_total = Math.floor(Math.random()*2+0.75)/2;
	let words = 30;
	window.localStorage.setItem("lastcallof_"+created,called);
	window.localStorage.setItem("historyof_"+created,JSON.stringify(JSON.parse(window.localStorage.getItem("historyof_"+created)).concat([called])));
	let newMetaItem = { words: words, remembered: remembered_total, called: called, created: created };
	window.localStorage.setItem("called_"+called,JSON.stringify(newMetaItem));
}

async function newEntries(diarylength) {
	for ( let i = 0; i < diarylength; i++ ) {
		await newEntry(i);
	}
	return diarylength;
}

async function randomTests(diarylength) {
	for ( let i = 0; i < diarylength; i++ ) {
		await randomTest();
	}
}

async function simulateUsage(diarylength) {
	diarylength = Math.max(11,diarylength);
	newEntries(diarylength).then( res => { console.log(res); randomTests(res); } );
}
