var changelog = [];
changelog["v1.6-beta"] = ["21.2.2023","<ul><li>interne Backups</li><li>Farbschemaänderungen</li></ul>"]
changelog["v1.5-beta"] = ["23.1.2023","erste Beta-Version</br><ul><li>neu:<ul><li>Verschlüsselung</li><li>Suchfunktion im Tagebuch</li><li>seltenere zufällige Tests</li><li>tägliche Erinnerung</li></ul></li><li>besser:<ul><li>Erlebnisauswahl beim Testen</li></ul></ul>"];
changelog["v1.0-v1.4"] = ["12.12.2022-5.1.2023","Alpha-Versionen"];
