function similarity_asym(string1,string2) {
	// compute maximal common substrings, sum length squares; compare to square of minimal string length
	// eg., abcd abcbcd, (3²+3²)/(4²) > 1; is this ok?
	// product of lgth: 18/24 < 1
	// two mcs of length a,b intersect in l charactersin string 1 => length1 > a+b-l, length2 > a+b-l , correct for intersection:
	// 2(a+b-l)^2-l^2
	// e.g abab, baba; mcs: aba, bab, 18>16,  
	
	//mcs starting with string1
	//throw no error if strings are empty: not really needed, is it?
	//string1 = string1 + ' ';
	//string2 = string2 + ' ';
	//
	string1 = string1.substr(0,1)+string1.substr(1).toLowerCase();
	string2 = string2.substr(0,1)+string2.substr(1).toLowerCase();
	var sim = 0;
	var _index = 0;
	while ( _index < string1.length ) {
		var contained = true;
		var _length = 0;
		var _index2new = 0;
		while ( contained ) {
			_index2 = _index2new;
			_length += 1;
			_index2new = string2.indexOf( string1.substr(_index,_length) );
			if ( _index2new == -1 || _length + _index > string1.length ) { contained = false; }		
		}
		sim += ( ( string1.length - _index ) * ( string1.length - _index ) - ( string1.length - _index - _length + 1 ) * ( string1.length - _index - _length + 1 ) ) * ( ( string2.length - _index2 ) * ( string2.length - _index2 ) - ( string2.length - _index2 - _length + 1 ) * ( string2.length - _index2 - _length + 1 ) ) / ( 1+Math.log(string1.length/string2.length)*Math.log(string1.length/string2.length) )
		_index += _length;
	}
	sim = sim/( (string1.length)*(string1.length)*(string2.length)*(string2.length) );
	return sim;
}

//the symmetric version
function similarity(string1,string2) { return Math.max(similarity_asym(string1,string2),similarity_asym(string2,string1)); }
