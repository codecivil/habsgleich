//make set timeouts tracable over functions...
var resetForm;

function _restoreThenOnLoad() {
	backup(false,_onLoad);
}

async function _onLoad() {
	//load master key
	let control = await makeMasterKeyAvailable();

	//parse settings
	parseSettings();
	
	//set correct submit label and show appropriate fields
	document.querySelector('#passwordForm').reset();
	let usecase = "";
	////case: password not yet set
	if ( 
		( ! window.sessionStorage.getItem("key") && ! window.localStorage.getItem("crypto") ) ||
		( window.sessionStorage.getItem("key") && window.localStorage.getItem("crypto") && JSON.parse(window.localStorage.getItem("crypto")).encrypted == "false" )
	) {
		usecase = "setpassword";
		strings['passwordsubmit'] = strings['nopasswordsubmit'];
		document.querySelector('#oldpassword_container').hidden = true;
		document.querySelector('#newpassword').addEventListener("keyup",comparePasswords,false);
		document.querySelector('#confirmnewpassword').addEventListener("keyup",comparePasswords,false);
		document.querySelector('#newpassword').focus();
	}
	////case: start of app
	if ( ! window.sessionStorage.getItem("key") && window.localStorage.getItem("crypto") && JSON.parse(window.localStorage.getItem("crypto")).encrypted == "true" ) {
		usecase = "login";
		strings['passwordsubmit'] = strings['loginsubmit'];
		strings['newpassword'] = strings['password'];
		document.querySelector('#oldpassword_container').hidden = true;
		document.querySelector('#confirmnewpassword_container').hidden = true;
		document.querySelector('#newpassword').addEventListener("keyup",_onSubmitPassword,false);
		document.querySelector('#passwordsubmit_container').hidden = true;
		document.querySelector('#newpassword').focus();
	}
	////case: no password used
	if ( ! window.sessionStorage.getItem("key") && window.localStorage.getItem("crypto") && JSON.parse(window.localStorage.getItem("crypto")).encrypted == "false" ) {
		usecase = "nopassword";
	}
	////case: password change
	if ( window.sessionStorage.getItem("key") && window.localStorage.getItem("crypto") && JSON.parse(window.localStorage.getItem("crypto")).encrypted == "true" ) {
		usecase = "changepassword";
		document.querySelector('#oldpassword').addEventListener("keyup",_onSubmitOldPassword,false);
		document.querySelector('#newpassword_container').hidden = true;
		document.querySelector('#confirmnewpassword_container').hidden = true;
		document.querySelector('#passwordsubmit_container').hidden = true;
		document.querySelector('#oldpassword').focus();
	}
	document.querySelector('.password.description').innerHTML = strings["text_"+usecase];
	window.sessionStorage.setItem("password_usecase",usecase);

	if ( usecase == "nopassword") { backup(false,function(){window.location = "index.html";}); }
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();
	
	//set and get page history
	var camefrom = window.sessionStorage.getItem("page");
	window.sessionStorage.setItem("page","encrypt.html");
	
	//set event listeners
	document.querySelector('#passwordsubmit').addEventListener("click",_onSubmitPassword,false);

	//show body
	document.body.style.opacity = "1";
}

function comparePasswords(e) {
	e.preventDefault();
	let usecase = window.sessionStorage.getItem("password_usecase");
	let initialLabel = document.querySelector('label[for=passwordsubmit]').textContent;
	let newpassword = document.querySelector('#newpassword').value;
	let confirmnewpassword = document.querySelector('#confirmnewpassword').value;
	let submitLabel = document.querySelector('label[for=passwordsubmit]');
	let submitButton = document.querySelector('#passwordsubmit');
	let submitDiv = document.querySelector('#passwordsubmit_container');
	if ( newpassword.length + confirmnewpassword.length == 0 ) {
		submitLabel.textContent = strings["nopasswordsubmit"];
		submitButton.disabled = false;
		submitDiv.classList.remove("disabled");
	}
	if ( newpassword.length + confirmnewpassword.length > 0 && newpassword.length == confirmnewpassword.length ) {
		submitLabel.textContent = strings[usecase+"submit"];
		submitButton.disabled = false;
		submitDiv.classList.remove("disabled");
	}
	if ( newpassword.length + confirmnewpassword.length > 0 && newpassword.length != confirmnewpassword.length ) {
		submitLabel.textContent = strings[usecase+"submit"];
		submitButton.disabled = true;
		submitDiv.classList.add("disabled");
	}
}

async function _onSubmitPassword(e) {
	e.preventDefault();
	//if ( typeof cordova == 'undefined' ) { return false; }
	let usecase = window.sessionStorage.getItem("password_usecase");
	let salt;
	let iv;
	let ivstring;
	let exported;
	let exportedString;
	let key;
	let _crypto;
	let masterkey;
	let masteriv;
	let masterivString;
	let ciphertext;
	let exportedBuffer;
	let keyArrayBuffer;
	let keyUint8Array;
	let cryptObj = new Object();
	let goOn = true;
	switch(usecase) {
		case "setpassword":	
		    //do not set if no passwords are given
		    if ( document.querySelector("#newpassword").value.length + document.querySelector('#confirmnewpassword').value.length == 0 ) {
				cryptObj.encrypted = "false";
				window.localStorage.setItem("crypto",JSON.stringify(cryptObj));
				backup(true,toIndex);
				break;
			}
			//create master key
			masterkey = await window.crypto.subtle.generateKey(
			  {
				name: "AES-GCM",
				length: 256
			  },
			  true,
			  ["encrypt", "decrypt"]
			);
			masteriv = window.crypto.getRandomValues(new Uint8Array(16));
			masterivString = JSON.stringify(masteriv);
			exported = await window.crypto.subtle.exportKey("jwk",masterkey);
			//create key for encryption of master key from password
			salt = await window.crypto.getRandomValues(new Uint8Array(16));
			saltstring = JSON.stringify(salt);
			iv = await window.crypto.getRandomValues(new Uint8Array(16));
			ivstring = JSON.stringify(iv);
			key = await getKey(document.querySelector('#newpassword'),salt);
			//encrypt masterkey and masteriv with key
			cryptObj.iv = ivstring;
			cryptObj.salt = saltstring;
			cryptObj.key = await encrypt(key,iv.buffer,JSON.stringify(exported)); //the masterkey encrypted by the password derived key
			cryptObj.masteriv = masterivString;
			cryptObj.encrypted = "true";
			//set localStorage item
			window.localStorage.setItem("crypto",JSON.stringify(cryptObj));
			//set sessionStorage item
			window.sessionStorage.setItem("key",JSON.stringify(exported));
			//encrypt existing adventures
			let _created = Object.keys(localStorage).filter(_key => _key.indexOf('created_') > -1 );
			if ( _created.length > 0 ) {
				document.querySelector('.progress').hidden = false;
				document.querySelector('#passwordForm').hidden = true;
				let _count = 0;
				let enc;
				for ( _key of _created ) {
					enc = await encrypt(masterkey,masteriv,localStorage.getItem(_key));
					localStorage.setItem(_key,enc);
					_count++;
					document.querySelector('.progress .finished').style.width = _count/_created.length + "%";
				};
			}
			backup(true,toIndex);
			break;
		case "login":
			_crypto = JSON.parse(window.localStorage.getItem("crypto"));
			salt = _array2buffer(JSON.parse(_crypto.salt));
			key = await getKey(document.querySelector('#newpassword'),salt);
			iv = _array2buffer(JSON.parse(_crypto.iv));
			exportedString = await decrypt(key,iv,_crypto.key);
			if ( exportedString == _crypto.key ) { return false; }
			window.sessionStorage.setItem("key",exportedString);
			toIndex();
			break;
		case "nopassword":
			cryptObj.encrypted = "false";
			window.localStorage.setItem("crypto",JSON.stringify(cryptObj)); backup(true);
			toIndex();
			break;
		case "changepassword":
			//clear timeouts
			if ( typeof resetForm !== 'undefined' ) { while ( resetForm > -1 ) { clearTimeout(resetForm); resetForm--; }; }
			_crypto = JSON.parse(window.localStorage.getItem("crypto"));
			//test if old password is correct
			salt = _array2buffer(JSON.parse(_crypto.salt));
			key = await getKey(document.querySelector('#oldpassword'),salt);
			iv = _array2buffer(JSON.parse(_crypto.iv));
			exportedString = await decrypt(key,iv,_crypto.key);
			if ( exportedString == _crypto.key ) { 
				document.querySelector('#oldpassword').placeholder = "falsches Passwort";
				resetForm = setTimeout(function(){document.querySelector('#passwordForm').reset();},3000);
				return false;
			}
			if ( exportedString != window.sessionStorage.getItem("key") ) {
				document.querySelector('#oldpassword').placeholder = "falsches Passwort";
				resetForm = setTimeout(function(){document.querySelector('#passwordForm').reset();},3000);
				return false;
			}
			//again, clear timeouts that may have arisen after initial clearance (because await took too long)
			if ( typeof resetForm !== 'undefined' ) { while ( resetForm > -1 ) { clearTimeout(resetForm); resetForm--; }; }
			//if password is empty: decrypt everything, unset key and crypto
			if ( document.querySelector('#newpassword').value == "" ) {
				masteriv = _array2buffer(JSON.parse(_crypto.masteriv));
				let _created = Object.keys(localStorage).filter(_key => _key.indexOf('created_') > -1 );
				if ( _created.length > 0 ) {
					document.querySelector('.progress').hidden = false;
					document.querySelector('#passwordForm').hidden = true;
					let _count = 0;
					_created.forEach( _key => {
						_getString(_key.replace('created_',''),false).then( res => localStorage.setItem(_key,res)	 );
						_count++;
						document.querySelector('.progress .finished').style.width = _count/_created.length + "%";
					});
				}
				window.sessionStorage.removeItem("key");
				cryptObj = new Object();
				cryptObj.encrypted = "false";
				window.localStorage.setItem("crypto",JSON.stringify(cryptObj));
				backup(true,toIndex);
				break;
			}
			//create key for encryption of master key from password
			iv = window.crypto.getRandomValues(new Uint8Array(16));
			ivstring = JSON.stringify(iv);
			key = await getKey(document.querySelector('#newpassword'),salt);
//later:			masterkey = await window.crypto.subtle.importKey("jwk",exported,false,["encrypt","decrypt"]);
//					masteriv = _array2buffer(JSON.parse(crypto.masteriv));
			//encrypt masterkey and masteriv with key
			cryptObj = new Object();
			cryptObj.iv = ivstring;
			cryptObj.salt = _crypto.salt;
			cryptObj.key = await encrypt(key,iv.buffer,exportedString); //the masterkey encrypted by the password derived key
			cryptObj.masteriv = _crypto.masteriv;
			cryptObj.encrypted = "true";
			//set new localStorage item
			window.localStorage.setItem("crypto",JSON.stringify(cryptObj));
			backup(true,toIndex);
			break;
		default:
			toIndex();
			break;
	}
	function toIndex() { window.location = "index.html"; }
	return false;
}

async function _onSubmitOldPassword(e) {
	e.preventDefault();
	if ( typeof resetForm !== 'undefined' ) { while ( resetForm > -1 ) { clearTimeout(resetForm); resetForm--; }; }
	_crypto = JSON.parse(window.localStorage.getItem("crypto"));
	//test if old password is correct
	salt = _array2buffer(JSON.parse(_crypto.salt));
	key = await getKey(document.querySelector('#oldpassword'),salt);
	iv = _array2buffer(JSON.parse(_crypto.iv));
	exportedString = await decrypt(key,iv,_crypto.key); 
	if ( exportedString == _crypto.key ) { 
		console.log("cant decrypt");
		document.querySelector('#oldpassword').placeholder = "falsches Passwort";
		resetForm = setTimeout(function(){document.querySelector('#passwordForm').reset();},3000);
		return false;
	}
	if ( exportedString != window.sessionStorage.getItem("key") ) {
		console.log("wrong key");
		document.querySelector('#oldpassword').placeholder = "falsches Passwort";
		resetForm = setTimeout(function(){document.querySelector('#passwordForm').reset();},3000);
		return false;
	}
	//again, clear Timeouts that may have arisen by awaits that took too long
	if ( typeof resetForm !== 'undefined' ) { while ( resetForm > -1 ) { clearTimeout(resetForm); resetForm--; }; }
	document.querySelector('#newpassword_container').hidden = false;
	document.querySelector('#confirmnewpassword_container').hidden = false;
	document.querySelector('#passwordsubmit_container').hidden = false;
	document.querySelector('#newpassword').focus();
	document.querySelector('#newpassword').addEventListener("keyup",comparePasswords,false);
	document.querySelector('#confirmnewpassword').addEventListener("keyup",comparePasswords,false);
	comparePasswords(new Event("click"));
	return false;
}

if ( NO_CRYPTO ) {
	window.location = "index.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready', _restoreThenOnLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_restoreThenOnLoad, false);
	}
}
