function _onLoad() {
	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();
	
	//clear tmp sessionStorage apart from key and password_usecase
	let tmpkey;
	let tmpusecase;
	if ( window.sessionStorage.getItem("key") ) {
		tmpkey = window.sessionStorage.getItem("key");
	}
	if ( window.sessionStorage.getItem("password_usecase") ) {
		tmpusecase = window.sessionStorage.getItem("password_usecase");
	}
	window.sessionStorage.clear();
	if ( typeof tmpkey != 'undefined' ) {
		window.sessionStorage.setItem("key",tmpkey);
	}
	if ( typeof tmpusecase != 'undefined' ) {
		window.sessionStorage.setItem("password_usecase",tmpusecase);
	}
	
	//fill summary entries
	let keys_called = Object.keys(localStorage).filter(key => key.indexOf('called_') > -1).sort();
	let keys_created = Object.keys(localStorage).filter(key => key.indexOf('created_') > -1);
	let numberofadventures = keys_created.length;
	let numberofcalls = keys_called.length - numberofadventures;
	let remembered = function() {
		let rem = 0;
		keys_called.forEach(key => {
			rem += parseFloat(JSON.parse(window.localStorage.getItem(key)).remembered);
		});
		return rem;
	}();
	let timespan;
	try { timespan = parseInt(keys_called[keys_called.length-1].replace('called_',''))-parseInt(keys_called[0].replace('called_','')); } catch(err) { timespan = 0; }
	timespan = Math.floor(timespan/86400000+0.5); timespan += " "+strings['days'];
	document.querySelector('.adventures').textContent = numberofadventures;
	document.querySelector('.tests').textContent = numberofcalls;
	document.querySelector('.remembered').textContent = remembered;
	document.querySelector('.timespan').textContent = timespan;
	
	document.querySelector('#summaryForm').addEventListener('submit',_onSubmitSummaryForm,false);

	window.sessionStorage.setItem("page","summary.html");

	//show body
	document.body.style.opacity = "1";
}


function _onSubmitSummaryForm(e) {
	e.preventDefault();	

	window.location = "index.html";
}



//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
