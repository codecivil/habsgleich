var searchTime;

async function _onLoad() {
	//load masterkey
	let control = await makeMasterKeyAvailable();
	
	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();
	
	//construct diary table
	if ( window.sessionStorage.getItem('needle') != null ) {
		document.querySelector('input.search').value = window.sessionStorage.getItem('needle');
	}
	searchNeedle(new Event("click"));
	
	document.querySelector('input.search').addEventListener("keyup",_onSearch,false);
	document.querySelector('span.remove').addEventListener("click",_onRemove,false);
	window.sessionStorage.setItem("page","diary.html");

	//show body
	document.body.style.opacity = "1";
}

function editAdventure(event) {
 let timestamp = event.target.closest("tr").dataset.timestamp;
 window.sessionStorage.setItem("edit",timestamp);
 window.location = "edit.html";
}

function _onRemove() {
	document.querySelector('input.search').value = '';
	document.querySelector('input.search').dispatchEvent(new Event("keyup"));
}

function _onSearch(e) {
	let lag = 2000;
	if ( document.querySelector('input.search').value.length > 2 ) {
		lag = 1000;
	}
	if ( typeof searchTime !== 'undefined' ) { while ( searchTime > -1 ) { clearTimeout(searchTime); searchTime--; }; }
	searchTime = setTimeout(searchNeedle,lag);
}

async function searchNeedle() {
	//save needle for session
	window.sessionStorage.setItem('needle',document.querySelector('input.search').value);
	//
	let needle = document.querySelector('input.search').value.split(' ');
	//compute table entry width
	let charwidth = parseFloat(document.querySelector('.measure').getBoundingClientRect().width);
	console.log(charwidth);
	let _numberofchars = Math.floor(parseFloat(window.getComputedStyle(document.querySelector('.container')).getPropertyValue('width').replace('px',''))/charwidth) - 13; //11: 10 for date (with full year), 1 for separator, 2 for padding
 	console.log(_numberofchars);
 	//construct diary table
	var diary_table = [];
	async function populateTable() {
		let _table = [];
		for (const key of Object.keys(localStorage).filter(key => (key.indexOf('created_') > -1)).sort().reverse()) {
			let plainEntry = await _getString(key.replace('created_',''),false);
			_table.push([key.replace('created_',''),plainEntry]);
		};
		return _table;
	}
	diary_table = await populateTable();
	document.querySelector('.diary').innerHTML = '';
// maybe do some kind of paging later
//	begin like: let diary_page = diary_table.splice(0,50);
	let diary_page = diary_table;
	diary_page.forEach(entry => {
		let _hit = true;
		needle.forEach( singleneedle => _hit = _hit && (entry[1].search(new RegExp(singleneedle,"i")) > -1 ) );
		if ( _hit ) {
			let _tr = document.createElement("tr");
			_tr.dataset.timestamp = entry[0];
			let _td1 = document.createElement("td");
				_td1.textContent = timestamp2readable(parseInt(entry[0]));
	//		 _td1.textContent = entry[0];
			let _td2 = document.createElement("td");
			_td2.textContent = entry[1].substring(0,_numberofchars);
			if ( entry[1].length > _numberofchars ) { _td2.textContent += "..."; }
			_tr.append(_td1);
			_tr.append(_td2);
			document.querySelector('.diary').append(_tr);
		}		
	});
	//add event listeners
	document.querySelectorAll('.diary tr').forEach(_tr => _tr.addEventListener('click', editAdventure,false) ); 
}

//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
