// returns the mind index at timestamp and some kind of standard error [mI,sd]
// takes into account last 100 days (so sizes of changes are indpendent of time)
function mindIndex(timestamp) {
	if ( timestamp == undefined ) { timestamp = Date.now(); } 
	let adventures_called = _get('called',[timestamp-100*86400000,timestamp]).sort();
	let adventures = [];
	adventures_called.forEach( adventure_called => {
		adventures.push(JSON.parse(window.localStorage.getItem(adventure_called)));
	});
	if ( adventures.length < 3 ) { return [0,0]; }
	let lastcalled = [];
	let mI = 0;
	let varplus = 0;
	let numberoftests = 0;
	adventures.forEach( (adventure) => {
		if ( lastcalled[adventure.created] == undefined ) { lastcalled[adventure.created] = adventure.called; numberoftests -= 1; }
		let summand = (adventure.called*adventure.remembered-lastcalled[adventure.created]*adventure.remembered)/86400000; //unit: days
		mI += summand;
		varplus += summand*summand;
		lastcalled[adventure.created] = adventure.called;
		numberoftests += 1;
	});
	if ( numberoftests == 0 ) {
		mI = 0; sd = 0;
	} else {
		mI = mI/numberoftests;
		_var = varplus/numberoftests - mI*mI;
		sd = Math.sqrt(_var);
	}
	return [mI,sd];
}

/* returns object
 * canGoOn: boolean
 * candidates: array("called_"+created times)
*/
function mindCanBeTested() {
	let [mI,sd] = mindIndex();
	//begin after BEGIN_AFTER_DAYS days or BEGIN_AFTER_ADVENTURES adventures; days:
	if ( mI == 0 && sd == 0 ) {
		mI = 2*BEGIN_AFTER_DAYS; sd = BEGIN_AFTER_DAYS;
	}
	sd = Math.max(MIN_SD,sd); // at least look MIN_SD days around the mI
	candidates = _get('candidates',[[Date.now()-mI*86400000-sd*86400000,0,4],[Date.now()-mI*86400000+sd*86400000,1,1000]]);
	let candObj = new Object();
	candObj.canGoOn = false;
	let testindex = 0;
	while ( ! candObj.canGoOn && testindex < TEST_REGIME.length ) {
		candidates = _get('candidates',[[Date.now()-mI*86400000+TEST_REGIME[testindex][0]*sd*86400000,0,4],[Math.min(Date.now()-MIN_LAG*86400000,Date.now()-mI*86400000+TEST_REGIME[testindex][1]*sd*86400000),1,1000]]);
		candObj.canGoOn = ( candidates.length > 0 );
		testindex++;
	}
	//begin after adventures
	if ( ! candObj.canGoOn ) {
		candidates = _get('candidates',[[Date.now()-BEGIN_AFTER_DAYS*86400000,0,4],[Date.now(),1,1000]]);
//		candidates = _get('candidates',[[Date.now()-BEGIN_AFTER_DAYS*86400000,0,1],[Date.now(),1,1000]]); //debug only!
		candObj.canGoOn = ( candidates.length >= BEGIN_AFTER_ADVENTURES );
	}
	candObj.candidates = candidates;
	return candObj;
}

//returns the created time of a suitable adventure
//storagename: optional source for array; if not given: compute from scratch
function testAdventure(storagename) {
	let candObj = new Object(); let cand;
	try {
		cand = JSON.parse(window.sessionStorage.getItem(storagename))
		candObj.candidates = cand;
		candObj.canGoOn = ( cand.length > 0 );
	} catch(err) {
		candObj = mindCanBeTested();
	}
	if ( ! candObj.canGoOn ) { return false; }
	rnd = Math.floor(Math.random()*candObj.candidates.length);
	try { created = JSON.parse(window.localStorage.getItem(candObj.candidates[rnd])).created; } catch(err) { return false; }
	return created;
}

//get all record keys with index index and key in the keyrange = [keylow,keyhigh] in the object store 'metadata' and optionally restricted to 
//elements of the array searchin;
function _get(index,keyrange_array,searchin) {
	let _keys;
	switch(index) {
		case 'called':
			_keys = Object.keys(localStorage).filter(key => key.indexOf("called_") > -1 && key.replace("called_","") >= keyrange_array[0] && key.replace("called_","") <= keyrange_array[1] ); 
			if (searchin) { return _keys.filter(key => searchin.includes(key)); } else { return _keys; }
			break;
		case 'candidates':
			function _otherfilters(key,keyrange_array) {
				let itemObj = JSON.parse(window.localStorage.getItem(key));
				//first test: is it the most recent call (exclude if not)
				if (
					window.localStorage.getItem("lastcallof_"+itemObj.created) == key.replace("called_","") &&
					itemObj.remembered >= keyrange_array[0][1] &&
					itemObj.remembered <= keyrange_array[1][1] &&
					itemObj.words >= keyrange_array[0][2] &&
					itemObj.words <= keyrange_array[1][2] 
				) { return true; }
				else { return false; }
			}
			_keys = Object.keys(localStorage).filter(key => 
				key.indexOf("called_") > -1 &&
				key.replace("called_","") >= keyrange_array[0][0] &&
				key.replace("called_","") <= keyrange_array[1][0] &&
				_otherfilters(key,keyrange_array)
				);
			if (searchin) { return _keys.filter(key => searchin.includes(key)); } else { return _keys; }
			break;
	}
}


//async functions

//returns string belonging to created time
//allowencrypted == true: returns ciphertext if it cannot be decrypted
//allowencrypted == false: returns standard failure string if it cannot be decrypted
async function _getString(created,allowencrypted) {
	let result
	//return plain entry if no password is set
	if ( typeof masterkey == "undefined" || typeof masteriv == "undefined" ) {
		result = window.localStorage.getItem("created_"+created);
	} else {
		result = await decrypt(masterkey,masteriv,window.localStorage.getItem("created_"+created));
	}
	if ( typeof allowencrypted != 'undefined' && allowencrypted == false ) {
		if ( result.indexOf(' ') == -1 ) { result = strings["decryptfailed"]; };
//		try { JSON.parse(result); result = strings["decryptfailed"]; } catch (err) {};
	}
	return result;
}

async function _putString(created,_string) {
	//return plain entry if no password is set
	if ( typeof masterkey == "undefined" || typeof masteriv == "undefined" ) {
		window.localStorage.setItem("created_"+created,_string);
	} else {
		let result = await encrypt(masterkey,masteriv,_string);
		window.localStorage.setItem("created_"+created,result);
	}
}

function writeTmp(key,string) {
	return new Promise ( (resolve,reject) => {
		window.sessionStorage.setItem(key,string);
		resolve(true);
	});
}

function readTmp(key) {
	return new Promise ( (resolve,reject) => {
		window.sessionStorage.getItem(key);
		resolve(true);
	});
}
