function _onLoad() {
	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();
	
	//generate the statistics graph
	generateStatistics();
	
	//add event listeners
	document.querySelector('.setting.back').addEventListener('click', _onBack,false);
	document.querySelector('.selectTimeRange').addEventListener('change',showNewStat,false);

	//show body
	document.body.style.opacity = "1";
}

async function fadeOut() {
	//hide body; normally, this is not necessary, but here _onLoad is a callback for changing the time range,
	//so it may be executed without reloading the page...
	document.body.style.opacity = "0";
}
async function fadeIn() {
	//hide body; normally, this is not necessary, but here _onLoad is a callback for changing the time range,
	//so it may be executed without reloading the page...
	document.body.style.opacity = "1";
}

function showNewStat() {
	fadeOut().then( ()=>{ setTimeout(function(){generateStatistics().then( ()=>{ fadeIn().then( ()=>{return true;} ); }); },100); } );
}
	
async function generateStatistics() {
	//compute historic mI values
	let mI_value = [];
	let mI_time = [];
	let mI = [];
	let firsttime = 0;
	let timeRange = parseInt(document.querySelector('.selectTimeRange').value);
	let timeLimit = Date.now() - timeRange*86400000;
	Object.keys(localStorage).filter(key => key.indexOf("called_") > -1 && parseInt(key.replace("called_","")) > timeLimit).sort().forEach(call => {
		if ( firsttime == 0 ) { firsttime = parseInt(call.replace("called_","")); }
		mI_time.push(parseInt(call.replace("called_",""))-firsttime);
		let [_mi,_sd] = mindIndex(parseInt(call.replace("called_","")));
		mI_value.push(_mi);
		mI.push([mI_time[mI_time.length-1],_mi, _sd]);
 	});
 	
 	//compute value and time ranges
 	let mI_time_min = 0;
 	let mI_time_range = Math.max(...mI_time);
 	let mI_value_min = Math.min(...mI_value);
 	let mI_value_range = Math.max(...mI_value)-mI_value_min;
 	let mI_min = [mI_time_min,mI_value_min];
 	let mI_range = [mI_time_range,mI_value_range];
 	let mI_max = [mI_min[0] + mI_range[0], mI_min[1] + mI_range[1]];

 	//do not show graphics if less then two values or no (partially succesful) tests
 	if ( mI.length < 2 || mI_max[1] + mI_min[1] == 0 ) {
		document.querySelector('svg').style.display = "none";
		document.querySelector('#selectTimeRange').style.display = "none";
		document.querySelector('.novalues').hidden = false;
		//add event listeners
		document.querySelector('.setting.back').addEventListener('click', _onBack,false);

		//show body
		document.body.style.opacity = "1";
		return false;
	}
	
 	//collect svg dimensions
 	let svg = document.querySelector('.app svg');
	svg.outerHTML = '<svg   xmlns:dc="http://purl.org/dc/elements/1.1/"    xmlns:cc="http://creativecommons.org/ns#"    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"    xmlns:svg="http://www.w3.org/2000/svg"    xmlns="http://www.w3.org/2000/svg"    xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"    xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"    width="192.01192mm"    height="163.2347mm"    viewBox="-8 0 192.01192 163.2347"    version="1.1"    id="svg8"    inkscape:version="1.0.2 (e86c870879, 2021-01-15)"    sodipodi:docname="statistics.svg">   <defs  id="defs2" />   <sodipodi:namedview  id="base"  pagecolor="#ffffff"  bordercolor="#666666"  borderopacity="1.0"  inkscape:pageopacity="0.0"  inkscape:pageshadow="2"  inkscape:zoom="0.35"  inkscape:cx="368.68992"  inkscape:cy="408.65226"  inkscape:document-units="mm"  inkscape:current-layer="layer1"  inkscape:document-rotation="0"  showgrid="false"  inkscape:window-width="1337"  inkscape:window-height="816"  inkscape:window-x="0"  inkscape:window-y="28"  inkscape:window-maximized="0" />   <metadata  id="metadata5"> <rdf:RDF>   <cc:Work  rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type    rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title>   </cc:Work> </rdf:RDF>   </metadata>   <g  inkscape:label="Ebene 1"  inkscape:groupmode="layer"  id="layer1"  transform="translate(-8.2841244,-40.04409)"> <rect    style="fill:#ffffff;stroke:#e6d13c;stroke-width:1.47;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"    id="rect001"    width="210"    height="162"    x="-8"    y="40"    ry="10.588889"    rx="10" /><rect    style="fill:none;stroke:#00000080;stroke-width:1.47;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"    id="rect833"    width="162.54575"    height="130.7832"    x="22.635178"    y="52.127888"    ry="10.588889"    rx="0" /> <path    style="fill:none;stroke:var(--statline);stroke-width:2;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"    d="M 33.232375,127.72428 63.46265,118.65105"    class="segment"    sodipodi:nodetypes="cc" /> <polygon    points="0,0"    fill="#00006b40"    class="sdarea" /> <rect    style="fill:none;stroke:none;stroke-width:2;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"    id="rect871"    width="192.01192"    height="157.2347"    x="8.2841244"    y="40.04409"    ry="10.588889" /> <text    xml:space="preserve"    style="font-style:normal;font-weight:normal;font-size:10.5833px;line-height:1.25;font-family:sans-serif;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.264583"    x="9.0087252"    y="59.681065"    id="text875"><tspan  sodipodi:role="line"  id="tspan873"  x="0"  y="59.681065"  style="stroke-width:0.264583">top</tspan></text> <text    xml:space="preserve"    style="font-style:normal;font-weight:normal;font-size:10.5833px;line-height:1.25;font-family:sans-serif;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.264583"    x="7.5138226"    y="180.62286"    id="text879"><tspan  sodipodi:role="line"  id="tspan877"  x="0"  y="180.62286"  style="stroke-width:0.264583">bottom</tspan></text> <text    xml:space="preserve"    style="font-style:normal;font-weight:normal;font-size:10.5833px;line-height:1.25;font-family:sans-serif;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.264583"    x="22.635178"    y="182.91107"    id="text883"><tspan  sodipodi:role="line"  id="tspan881"  x="17.635178"  y="195.91107"  style="stroke-width:0.264583">begin</tspan></text> <text    xml:space="preserve"    style="font-style:normal;font-weight:normal;font-size:10.5833px;line-height:1.25;font-family:sans-serif;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.264583"    x="185.18091"    y="182.91107"    id="text887"><tspan  sodipodi:role="line"  id="tspan885"  x="170.18091"  y="195.91107"  style="stroke-width:0.264583">end</tspan></text>   </g> </svg>';
	svg = document.querySelector('.app svg');
	let topleft = [parseFloat(svg.querySelector('#rect833').getAttribute("x")),parseFloat(svg.querySelector('#rect833').getAttribute("y"))];
	let dimensions = [parseFloat(svg.querySelector('#rect833').getAttribute("width")),parseFloat(svg.querySelector('#rect833').getAttribute("height"))];
 	
 	//compute coordinates of mI point in svg
 	function coord(mIpoint) {
		let _return = [];
		_return[0] = topleft[0] + (mIpoint[0]-mI_min[0])*dimensions[0]/mI_range[0];
		_return[1] = topleft[1] + (mI_max[1]-mIpoint[1])*dimensions[1]/mI_range[1];
		return _return;
	}
	
 	//compute line segments
 	let template = svg.querySelector('.segment');
 	let templatearea = svg.querySelector('.sdarea');
 	let oldcoord = coord(mI_min);
 	let oldtop = coord(mI_min);
 	let oldbottom = coord(mI_min);
 	mI.forEach(mIpoint => {
		let svgcoord = coord([mIpoint[0],mIpoint[1]]);
		let svgtop = coord([mIpoint[0],mIpoint[1]+mIpoint[2]]);
		let svgbottom = coord([mIpoint[0],mIpoint[1]-mIpoint[2]]);
		let segment = template.cloneNode(true);
		segment.setAttribute("d","M "+oldcoord[0]+","+oldcoord[1]+" L "+svgcoord[0]+","+svgcoord[1]);
		svg.querySelector('g').append(segment);
		let sdarea = templatearea.cloneNode(true);
		sdarea.setAttribute('points',oldtop[0]+","+oldtop[1]+" "+svgtop[0]+","+svgtop[1]+" "+svgbottom[0]+","+svgbottom[1]+" "+oldbottom[0]+","+oldbottom[1]);	
		svg.querySelector('g').append(sdarea);
		oldcoord = JSON.parse(JSON.stringify(svgcoord));
		oldtop = JSON.parse(JSON.stringify(svgtop));
		oldbottom = JSON.parse(JSON.stringify(svgbottom));
	});
	svg.querySelector("path").remove();
	svg.querySelector("#tspan873").textContent = parseInt(mI_max[1]*100+0.5)/100;
	svg.querySelector("#tspan877").textContent = parseInt(mI_min[1]*100+0.5)/100;
	svg.querySelector("#tspan881").textContent = timestamp2readable(firsttime);
	svg.querySelector("#tspan885").textContent = timestamp2readable(firsttime+mI_max[0]);
}

//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
