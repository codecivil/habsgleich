function _onLoad() {
	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();	

	//show body
	document.body.style.opacity = "1";
	
	//set page name
	window.sessionStorage.setItem("page","info.html");

}

//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
