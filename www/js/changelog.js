function _onLoad() {
	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();	

	//show body
	document.body.style.opacity = "1";

	//add event listeners
	document.querySelector('.setting.back').addEventListener('click', _onBack,false);
	
	//show the changelog strings
	Object.keys(changelog).sort().reverse().forEach( _log => {
		let _li = document.createElement('li');
		_li.innerHTML = "<h3>"+_log+":</h3><h4>"+changelog[_log][0]+"</h4><p>"+changelog[_log][1]+"</p>";
		document.querySelector('ul.info').append(_li);
	});
}

//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
