//version with or without crypto (for Google Play if the US changes its laws again...)
const NO_CRYPTO = false;

//Is this for Google Play? (If yes, remove donation links, e.g.)
const GOOGLEPLAY = true;

// begin mind testing after these number of days
const BEGIN_AFTER_DAYS = 5; 

// begin mind testing after these number of entries
const BEGIN_AFTER_ADVENTURES = 5;

// smallest interval in days around mind index to look for testable entries
const MIN_SD = 2;

// minimal timelag between adventure and test in days
const MIN_LAG = 1;

//TEST_REGIME: array of sd intervals around mI to look for test adventures successively while no testable adventures are found
const TEST_REGIME = [[-1,0],[-2,0],[-10,1],[-20,1],[-50,1],[-100,1],[-200,1],[-500,1]];

//Remind to add an adventure after milliseconds
const REMINDME = 86400000;
// const REMINDME = 8640;
//debug settings
//~ const BEGIN_AFTER_DAYS = 0; // begin mind testing after these number of days
//~ const BEGIN_AFTER_ADVENTURES = 0; // begin mind testing after these number of entries
//~ const MIN_SD = 2; // smallest interval in days around mind index to look for testable entries
//~ const MIN_LAG = 0; // minimal timelag between adventure and test in days
//~ //TEST_REGIME: array of sd intervals around mI to look for test adventures successively while no testable adventures are found
//~ const TEST_REGIME = [[-500,0],[-2,0],[-5,1],[-500,1]];
