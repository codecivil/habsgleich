function _onLoad() {
	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();
	
	//show old and new indices
	let [rem,oldmI] = [1,0];
	try { [rem,oldmI] = JSON.parse(window.sessionStorage.getItem("forResults")); } catch(err) { [rem,oldmI] = [1,0]; }
	if ( oldmI == "n.a." ) { oldmI = 0; }
//	let [rem,oldmI] = [0.5,3.8];
	let h1string;
	switch(rem) {
		case 0:
			h1string = strings["testfailed"];
			break;
		case 0.5:
			h1string = strings["testpartlyfailed"];
			break;
		case 1:
			h1string = strings["testsuccessful"];
			break;		
	}
	document.querySelector("h1.result").textContent = h1string;
	let [newmI,sd] = mindIndex();
	newmI = Math.floor(newmI*10+0.5)/10; if ( newmI.toString().indexOf('.') == -1 ) { newmI = newmI.toString()+'.0'; };
	if ( oldmI.toString().indexOf('.') == -1 ) { oldmI = oldmI.toString()+'.0'; };
	sd = Math.floor(sd*10+0.5)/10; if ( sd.toString().indexOf('.') == -1 ) { sd = sd.toString()+'.0'; };
	if ( newmI > oldmI) { document.querySelector('.mIdirection').classList.add("up"); }
	if ( newmI == oldmI) { document.querySelector('.mIdirection').classList.add("equal"); }
	if ( newmI < oldmI) { document.querySelector('.mIdirection').classList.add("down"); }
	document.querySelector('.oldmindindex').textContent = oldmI;
	document.querySelector('.newmindindex').textContent = newmI;
	document.querySelector('.newsd').innerHTML = "(&plusmn;"+sd+")";
	
	document.querySelector('#resultsForm').addEventListener('submit',_onSubmitResultsForm,false);

	window.sessionStorage.setItem("page","results.html");

	//show body
	document.body.style.opacity = "1";
}


function _onSubmitResultsForm(e) {
	e.preventDefault();	

	window.location = "summary.html";
}



//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
