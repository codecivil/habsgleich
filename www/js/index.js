async function _onLoad() {
	//load master key
	let control = await makeMasterKeyAvailable();

	//parse settings
	parseSettings();
	
	//fill textContents by strings variable
	fillHTML();
	
	//get last entry and mind index
	fillTopBar();

	//remove tmo localStorage
	window.localStorage.removeItem("tmp");

	const adventure_string = document.querySelector('textarea[form=adventureForm]');
	document.querySelector('#adventureForm').addEventListener('submit',_onSubmitAdventureForm,false);
	document.querySelector('#continueForm').addEventListener('submit',_onSubmitContinueForm,false);
	window.sessionStorage.setItem("page","index.html");
	
	//show body
	document.body.style.opacity = "1";
}

async function _onSubmitAdventureForm(e) {
	e.preventDefault();	
	//essentialstring: replace blacklist words and words with less then 4 characters
	const adventure_string = document.querySelector('textarea[form=adventureForm]');
	let words = essentialWords(adventure_string.value).length;
	let called = Date.now();
	let remembered = 0;
	//insert into localStorage ("created", "called" and "lastcallof" prefixes)
	_putString(called,adventure_string.value);
	window.localStorage.setItem("lastcallof_"+called,called);
	window.localStorage.setItem("historyof_"+called,JSON.stringify([called]));
	let newMetaItem = { words: words, remembered: remembered, called: called, created: called };
	window.localStorage.setItem("called_"+called, JSON.stringify(newMetaItem));
	//what to do after successful inserts:
	//test in about half the cases, so you have enough untested adventures for index growth later; 
	//otherwise you will converge to the global settings...
	let willtest = Math.floor(Math.random()+0.5);
	let tmp = new Object();
	tmp.canGoOn = false;
	if ( willtest == 1 ) {
		tmp = await mindCanBeTested();
	}
	if ( tmp.canGoOn ) {
		writeTmp("forTest",JSON.stringify(tmp.candidates)).then( result => { backup(true,function(){window.location = "test.html"; }); } );
	} else { backup(true,function(){window.location = "summary.html"; }); }
}

function _onSubmitContinueForm(e) {
	e.preventDefault();
	if ( mindCanBeTested().canGoOn ) { window.location = "test.html"; } else { window.location = "summary.html"; }
}

//go to encrypt.html if no key is set:
if ( ! NO_CRYPTO && ! window.sessionStorage.getItem("key") && window.sessionStorage.getItem("password_usecase") != "nopassword" ) {
	window.location = "encrypt.html";
} else {
	if ( typeof cordova !== 'undefined' ) {
		window.addEventListener('DOMContentLoaded', function () {
			document.addEventListener('deviceready',_onLoad, false);
		}, false);
	} else {
		window.addEventListener('DOMContentLoaded',_onLoad, false);
	}
}
