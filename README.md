# HabsGleich

## Name
HabsGleich is german and means vaguely JustASec.

## Description
HabsGleich is a mini diary with an included mind test. It is designed to detect changes in your mind performance without
referring to non-personal standard tests or submitting sensitive data to third-party servers. This repository contains the source code of the cordova based android app.

## Documentation
For a complete documentation and ways to use this app, please refer to [https://habsgleich.docs.codecivil.de](https://habsgleich.docs.codecivil.de) (in German only).

## License
GPLv3 or later.

## Project status
v1.5-beta is the first beta release.
