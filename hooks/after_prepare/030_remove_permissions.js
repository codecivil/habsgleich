#!/usr/bin/env node

//
// This hook removes specific permissions from the AndroidManifest.xml
// The AndroidManifest is re-generated during the prepare stage,
// so this must be run on the "after_prepare" hook.
//


// Configure the permissions to be forcefully removed.
// NOTE: These permissions will be removed regardless of how many plugins
//       require the permission. You can check the permission is only required
//       by the plugin you *think* needs it, by looking at the "count" shown in
//       your /plugins/android.json file.
//       If the count is more than 1, you should search through
//       the /plugins//plugin.xml files for <uses-permission> tags.

var fs = require('fs');

if(fs.existsSync('platforms/android')) {
  const PERMISSIONS_TO_REMOVE = ["INTERNET"],
    MANIFEST = 'platforms/android/AndroidManifest.xml';

  manifestLines = fs.readFileSync(MANIFEST).toString().split('\n'),
    newManifestLines = [];

  const permissions_regex = PERMISSIONS_TO_REMOVE.join('|');

  manifestLines.forEach(function(line) {
    if(!line.match(permissions_regex)) {
      newManifestLines.push(line);
    }
  });

  fs.writeFileSync(MANIFEST, newManifestLines.join('\n'));
}
